<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>

<body>
<div id="header">
  <div id="logotop">
  <?php if ($site_name): ?>
    <h1 id='site-name'>
    <a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>">
    <?php print $site_name; ?>
    </a>
    </h1>
  <?php endif; ?>
        
  <?php if ($site_slogan): ?>
    <h2 id='site-slogan'>
    <?php print $site_slogan; ?>
    </h2>
  <?php endif; ?>
  </div>
</div>

  <?php if ($primary_links): ?>
    <div id="primary">
    <?php print theme('menu_links', $primary_links); ?>
    </div>
  <?php endif; ?>
  
<div id="content">
	<div id="main">
		<div id="missionbox">
		<?php if ($mission): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
		</div>
        <?php print $breadcrumb; ?>
		<?php if ($title): ?><h1 class="title"><?php print $title; ?></h1><?php endif; ?>
        <?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
        <?php print $help; ?>
        <?php print $messages; ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?>
	</div>
    
        <?php if ($sidebar_right): ?>
	    <div id="sidebar">
        <?php if ($logo): ?>
          <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo" />
          </a>
        <?php endif; ?>
	    <?php print $sidebar_right; ?>
	    </div>
        <?php endif; ?>
</div>

<div id="footer">
    <?php print $footer_message; ?>
	<p id="themed">Themed by <a href="http://anthonylicari.com" title="Anthony Licari">Anthony Licari</a>.</p>
	<?php print $closure; ?>
</div>
</body>
</html>
